package me.korzeniak.libs.commons.date;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

class DateConverterTest {

    private static final int SYSTEM_DEFAULT_OFFSET = 2;

    @BeforeAll
    public static void setupZone() {
        TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.ofHours(SYSTEM_DEFAULT_OFFSET)));
    }

    @Test
    void ConvertOffsetToLocalDate_NullInput_NullOutput() {
        //given
        OffsetDateTime offsetDateTime = null;

        //when
        LocalDateTime localDateTime = DateConverter.toLocalDateTime(offsetDateTime);

        //then
        assertNull(localDateTime);
    }


    @Test
    void ConvertOffsetToLocalDate() {
        //given
        int zoneDifference = 3;
        OffsetDateTime offsetDateTime = OffsetDateTime.of(
                LocalDate.of(2020, 5, 12),
                LocalTime.of(6, 7, 8),
                ZoneOffset.ofHours(SYSTEM_DEFAULT_OFFSET - zoneDifference));

        //when
        LocalDateTime result = DateConverter.toLocalDateTime(offsetDateTime);

        //then
        LocalDateTime expected = LocalDateTime.of(2020, 5, 12, 9, 7, 8);
        assertEquals(expected, result);
    }

    @Test
    void toOffsetDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        OffsetDateTime offsetDateTime = DateConverter.toOffsetDateTime(localDateTime);
        assertNotNull(offsetDateTime);
    }
}