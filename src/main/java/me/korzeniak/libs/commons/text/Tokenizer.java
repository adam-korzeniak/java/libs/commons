package me.korzeniak.libs.commons.text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer {

    private Tokenizer() {}

    private static final String TOKENIZE_WITH_DELIMITERS_PATTERN = "([^%s]+)|([%s])";

    public static List<String> tokenizeWithDelimiters(String text, String delimiters) {
        List<String> tokens = new ArrayList<>();

        String delimiterRegex = Pattern.quote(delimiters);
        String patternString = String.format(TOKENIZE_WITH_DELIMITERS_PATTERN, delimiterRegex, delimiterRegex);

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            tokens.add(matcher.group());
        }

        return tokens;
    }
}
