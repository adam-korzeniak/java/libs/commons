package me.korzeniak.libs.commons.file;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReader {

    private FileReader() {}

    public static String readFileContent(String filePath) throws IOException {
        Stream<String> stream = readFileStream(filePath);
        return stream.collect(Collectors.joining(System.lineSeparator()));
    }

    public static Stream<String> readFileStream(String filePath) throws IOException {
        return Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
    }
}